local elementCount = 0
local luaStack = {}

local function pushElementToStack(value)
   table.insert(luaStack, value)
   elementCount = elementCount + 1
end

local function popElementFromStack()
    elementCount = elementCount - 1
   return table.remove(luaStack)
end

local function peekElement()
    res = popElementFromStack()
    pushElementToStack(res)
    return res
 end

 local function isNotEmptyStack()
    return elementCount >= 1
 end



 function getPriority(c)
    if(c=='(') then
        return 1 
    elseif(c == '+' or c == '-') then
        return 2
    elseif(c == '*' or c == '/') then
        return 3
    end
    return -1
end


function InfixToPostfix(input_string)
    input_length = string.len(input_string);
    result = string.char()
    prevLocation = -1
    for i = 1,input_length,1
    do
        currentc = string.sub(input_string,i,i)
        if (currentc == '(') then
            pushElementToStack(currentc)
        elseif (currentc == ')') then
            topToken = peekElement()
            popElementFromStack()
            while(topToken ~= '(')
            do
                result = result..' '..currentc
                topToken = peekElement()
                popElementFromStack()
            end
        elseif((currentc >= 'a' and currentc <= 'z') or (currentc >= 'A' and currentc <= 'Z') or (currentc>='0' and currentc<='9')) then
            if(prevLocation+1 == i) then
                result = result..currentc
            else
                result = result..' '..currentc
            end
            prevLocation = i
        elseif (currentc==' ' or currentc=='\t') then
        else
            while(isNotEmptyStack() and getPriority(peekElement()) >= getPriority(currentc))
            do
                result = result..' '..peekElement()
                popElementFromStack()
            end
            pushElementToStack(currentc)
        end
    end

    while(isNotEmptyStack())
    do
        result = result..' '..peekElement()
        popElementFromStack() 
    end

    return result
end
print('Assignment #3-2, haibo zhou, 1050029976@qq.com')
