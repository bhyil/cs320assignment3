#include <iostream>
extern "C"
{
    #include "lua.h"
    #include "lauxlib.h"
    #include "lualib.h"
}
void showLuaError(lua_State *state);
void runLuaFile(const char *filename);
int main(int argc, char **argv)
{
    std::cout << "Assignment #3-3, haibo zhou, 1050029976@qq.com" << std::endl;
    if (argc < 2)
    {
        std::cout<<"file is missing!"<<std::endl;
        return 1;
    }
    runLuaFile(argv[1]);
    return 0;
}

void showLuaError(lua_State *state)
{
    // The error message is on top of the stack.
    // Fetch it, print it and then pop it off the stack.
    const char *message = lua_tostring(state, -1);
    if(message)
        std::cout << "run error:" << message << std::endl;
    lua_pop(state, 1);
}

void runLuaFile(const char *filename)
{
    lua_State *state = luaL_newstate();
    // Make standard libraries available in the Lua object
    luaL_openlibs(state);
    int result;
    // Load the program; this supports both source code and bytecode files.
    result = luaL_loadfile(state, filename);
    if (result != LUA_OK)
    {
        showLuaError(state);
        return;
    }
    // Finally, runLuaFile the program by calling into it.
    // Change the arguments if you're not running vanilla Lua code.
    result = lua_pcall(state, 0, LUA_MULTRET, 0);
    if (result != LUA_OK)
    {
        showLuaError(state);
        return;
    }
    std::string line;
    //input the string
    std::getline(std::cin, line);

    std::string test = "return InfixToPostfix(\'"+line+"\')";
    //call lua script in c++
    luaL_dostring(state,test.c_str());
    //check string
    std::cout << luaL_checkstring(state,1) << std::endl;

    //close the lua environment
    lua_close(state);

}